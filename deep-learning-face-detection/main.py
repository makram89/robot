# python main.py --prototxt deploy.prototxt.txt --model res10_300x300_ssd_iter_140000.caffemodel

# import the necessary packages
from imutils.video import VideoStream
import numpy as np
import argparse
import imutils
import time
import cv2
import serial

# import object_tracking.py as tr


# construct the argument parse and parse the arguments
ap = argparse.ArgumentParser()
ap.add_argument("-p", "--prototxt", required=True,
                help="path to Caffe 'deploy' prototxt file")
ap.add_argument("-m", "--model", required=True,
                help="path to Caffe pre-trained model")
ap.add_argument("-c", "--confidence", type=float, default=0.95,
                help="minimum probability to filter weak detections")
ap.add_argument("-v", "--video", required=False,
                help="path to input video file")
args = vars(ap.parse_args())



ser = serial.Serial(
    port='COM7',
    baudrate=9600,
    parity=serial.PARITY_NONE,
    stopbits=serial.STOPBITS_ONE,
    bytesize=serial.EIGHTBITS,
    timeout=1
)

# loop over the frames from the video stream


pozycja = np.array((320, 240))
ulamki = np.array((0, 0))
step = 0


def steruj_pozycja(nowa_pozycja):
    global pozycja
    global ulamki
    global step
    step = (step + 1) % 2

    if any(pozycja < 0) or pozycja[0] > 640 or pozycja[1] > 480: return

    roznica_pozycji = nowa_pozycja - pozycja
    roznica_pozycji_w_krokach = roznica_pozycji / 10
    # roznica_pozycji_w_krokach += ulamki
    calkowite = roznica_pozycji_w_krokach.round(0)
    # ulamki = roznica_pozycji_w_krokach - calkowite

    if not step:
        roznica_pozycji_w_krokach = calkowite
        return
    else:
        roznica_pozycji_w_krokach += calkowite
        roznica_pozycji_w_krokach //= 2

    if roznica_pozycji_w_krokach[0] > 0:
        wysylanie_do_arduino(2000 + roznica_pozycji_w_krokach[0])
    elif roznica_pozycji_w_krokach[0] < 0:
        wysylanie_do_arduino(1000 - roznica_pozycji_w_krokach[0])

    if roznica_pozycji_w_krokach[1] > 0:
        wysylanie_do_arduino(4000 + roznica_pozycji_w_krokach[1])
    elif roznica_pozycji_w_krokach[1] < 0:
        wysylanie_do_arduino(3000 - roznica_pozycji_w_krokach[1])

    # pozycja = nowa_pozycja


def srodek_prosotkata(coordy):
    sx, sy, ex, ey = coordy
    return np.array(((sx + ex), (sy + ey))) / 2


def wysylanie_do_arduino(value):
    # if ser.isOpen():
    #     print(ser.name + ' is open...')
    val = str(int(value))
    print(val)
    ser.write(str.encode(val))
    # time.sleep(0.01)


def przetwarzanie_obrazu():

    # load our serialized model from disk
    print("[INFO] loading model...")
    net = cv2.dnn.readNetFromCaffe(args["prototxt"], args["model"])

    # initialize the video stream and allow the cammera sensor to warmup
    print("[INFO] starting video stream...")
    vs = VideoStream(args["video"]).start()
    # vs = VideoStream(src=0).start()

    time.sleep(2.0)

    cv2.namedWindow('Frame', cv2.WINDOW_NORMAL)
    # tracker = cv2.TrackerMedianFlow_create()

    while True:
        # grab the frame from the threaded video stream and resize it

        frame = vs.read()
        # To make it faster
        frame = imutils.resize(frame, width=640)

        (h, w) = frame.shape[:2]
        blob = cv2.dnn.blobFromImage(cv2.resize(frame, (300, 300)), 1.0, (300, 300), (104.0, 177.0, 123.0))

        # pass the blob through the network and obtain the detections and
        # predictions
        net.setInput(blob)
        detections = net.forward()

        for i in range(0, detections.shape[2]):
            # extract the confidence (i.e., probability) associated with the
            # prediction

            confidence = detections[0, 0, i, 2]

            # filter out weak detections by ensuring the `confidence` is
            # greater than the minimum confidence
            if confidence < args["confidence"]:
                continue


            # compute the (x, y)-coordinates of the bounding box for the
            # object
            box = detections[0, 0, i, 3:7] * np.array([w, h, w, h])

            # mamy coordy, zapsiac je do tupla coordy
            coordy = (startX, startY, endX, endY) = box.astype("int")
            # wyciagniete koordynaty boxa

            # draw the bounding box of the face along with the associated
            # probability
            text = "{:.2f}%".format(confidence * 100)
            y = startY - 10 if startY - 10 > 10 else startY + 10
            cv2.rectangle(frame, (startX, startY), (endX, endY),
                          (0, 0, 255), 2)
            cv2.putText(frame, text, (startX, y),
                        cv2.FONT_HERSHEY_SIMPLEX, 0.45, (0, 0, 255), 2)
            # print((startX, startY))

            crd = srodek_prosotkata(tuple(coordy))
            steruj_pozycja(crd)
            break

        # show the output frame
        cv2.imshow("Frame", frame)
        key = cv2.waitKey(1) & 0xFF
        # if the `q` key was pressed, break from the loop
        if key == ord("q"):
            return

    # do a bit of cleanup
    cv2.destroyAllWindows()
    vs.stop()


if __name__ == '__main__':
    przetwarzanie_obrazu()
