from getch import getch
import serial

ser = serial.Serial(
    port='/dev/ttyACM0',
    baudrate=9600,
    parity=serial.PARITY_NONE,
    stopbits=serial.STOPBITS_ONE,
    bytesize=serial.EIGHTBITS,
    timeout=1
)

if __name__ == '__main__':
    while True:
        _ = getch()
        _ = getch()  # just get rid of two first reundant codes
        c = ord(getch())

        if c == 65:
            ser.write("3005".encode())
        elif c == 66:
            ser.write("4005".encode())
        elif c == 67:
            ser.write("2005".encode())
        elif c == 68:
            ser.write("1005".encode())
